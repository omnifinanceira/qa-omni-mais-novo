require_relative 'util.rb'

class OmniFacil < Util

    set_url 'https://hml-omnifacil2.omni.com.br/hml/pck_login.prc_login'

    element :elementinputusuario, :xpath, "//input[@id='p-nome']"
    element :elementinputsenha, :xpath, "//input[@id='p-senha']"
    element :elementconectar, :xpath, "//button[@id='btn-conectar']"
    element :elementhabilitarlistaagente, :xpath, "//span[@role='combobox']"
    element :elementselecionaragente, :xpath, "//li[text()='184 - BATISTELLA - MARINGA-PR']"
    element :elementvalidar, :xpath, "//button[@id='bt-validar']"
    element :elemetmenu, :xpath, "//*[@id='navbar-collapse-1']/ul[1]/li[2]/ul/li[5]/ul/li[3]/a"
    element :elementfontordenarproposta, :xpath, "//font[@title='Ordenação crescente pelo Número da Proposta']"

    def logar_usuario(usuario = $usuarioagente,senha = $senhaagente)
        elementinputusuario.set usuario
        elementinputsenha.set senha
        elementconectar.click
    end

    def selecionar_menu_inicial(nome_menu)
        find(:xpath, "//*[text()='#{nome_menu}']").click
    end

    def listar_agente
        elementhabilitarlistaagente.click
        elementselecionaragente.click
        elementvalidar.click
    end

    def selecionar_menu(menu = nil)
        if !menu.nil?
            find(:xpath, "//li/a[text()=' #{menu}']").click
        end
    end

    def selecionar_submenu(submenu = nil)
        if !submenu.nil?
            find(:xpath, "//ul/li/a[text()='#{submenu}']").click 
        end
    end

    def marcar_proposta_omni_mais
        within_frame(find(:xpath, "//iframe")[:id]) do
            within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[0][:name]) do
                find(:xpath, "//input[@id='chk-mostra']").click
            end
        end
    end

    def consultar_ficha(numero_proposta)
        #proposta em preenchimento
        @erro = ""
        within_frame(find(:xpath, "//iframe")[:id]) do
            within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
                begin
                    page.send_keys [:control, :end]
                    find(:xpath, "//input[contains(@onclick,'#{numero_proposta}')]").click
                rescue Exception => ex
                    @erro = ex.message
                end
            end
        end

        return @erro
    end

    def acessar_etapa(nome_etapa)
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[@value='#{nome_etapa}']").click
        end
    end

    def consulta_historica_avalista
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[contains(@value, 'Avalista']").click
        end
    end

    def aceitar_etapa
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[contains(@onclick, 'APROVADA')]").click
            page.accept_alert
        end
    end

    def pegar_valor_prazo_omnifacil
        @dados_proposta_sucesso = []

        within_frame(find(:xpath, "//iframe")[:id]) do
            @dados_proposta_sucesso << find(:xpath, "//input[@id='vl_prest']")[:value]
            @dados_proposta_sucesso << find(:xpath, "//input[@id='P_PROP_NUM_PREST']")[:value]
        end

        puts "OmniFacil - Proposta: " +  $numero_proposta + " Prazo: " + @dados_proposta_sucesso[1] + ' ' + "Valor: " + @dados_proposta_sucesso[0]
        return @dados_proposta_sucesso
    end

    def clicar_gravar
        send_keys [:control, :end]
        within_frame(find(:xpath, "//iframe")[:id]) do
            while !find(:xpath, "//input[@value='Gravar']").visible?
            end

            find(:xpath, "//input[@value='Gravar']").click
            page.accept_alert
        end
    end

    def enviar_mesa_omni
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[@value='Enviar Mesa Omni']").click
            page.accept_alert
        end
    end

    def preencher_checklist
        within_frame(find(:xpath, "//iframe")[:id]) do
            find(:xpath, "//input[contains(@value, 'teste')]").click
            all(:xpath, "//textarea[@class='texto']").each{|i| i.set("QA automação teste")}

            find(:xpath, "//input[@value='Gravar']").click

            accept_confirm("Deseja enviar a proposta #{$numero_proposta} para a Mesa Omni?")
            dismiss_confirm("Deseja incluir/alterar o checklist antes de enviar a proposta para a Mesa Omni?")
        end
    end

    def acessar_grupo_proposta_mesa(cod_mesa_proposta)
        mesa = cod_mesa_proposta[0][3] + "_" + cod_mesa_proposta[0][2]
        within_frame(find(:xpath, "//iframe")[:id]) do
            @nova_janela = window_opened_by {
                find(:xpath, "//div[@id='fila#{mesa}']").find(:xpath, "//div[@id='infoEspera#{mesa}']/a").click
            }
        end
    end

    def pop_up_alerta_mesa
        begin
            within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
                if find(:xpath, "//div[@id='popup_container']").visible?
                    find(:xpath, "//*[@id='popup_ok']").click
                end
            end
        rescue Exception => ex
         ex.message
        end
    end

    def preencher_procedencia
        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            all(:xpath, "//input[@name='P_PROCEDE_RES']")[1].click
            find(:xpath, "//input[@name='P_EM_NOME_RES']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_QUAL_END_RES']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_CONTATO_RES']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_RELACAO_RES']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//textarea[@name='P_INFORMACAO_RES']").set('QA TESTE AUTOMAÇÃO')

            all(:xpath, "//input[@name='P_PROCEDE_COM']")[1].click
            find(:xpath, "//input[@name='P_EM_NOME_COM']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_QUAL_END_COM']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_CONTATO_COM']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_RELACAO_COM']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//textarea[@name='P_INFORMACAO_COM']").set('QA TESTE AUTOMAÇÃO')

            all(:xpath, "//input[@name='P_PROCEDE_FAM1']")[1].click
            find(:xpath, "//input[@name='P_EM_NOME_FAM1']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_QUAL_END_FAM1']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_CONTATO_FAM1']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_RELACAO_FAM1']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//textarea[@name='P_INFORMACAO_FAM1']").set('QA TESTE AUTOMAÇÃO')

            all(:xpath, "//input[@name='P_PROCEDE_FAM2']")[1].click
            find(:xpath, "//input[@name='P_EM_NOME_FAM2']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_QUAL_END_FAM2']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_CONTATO_FAM2']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//input[@name='P_RELACAO_FAM2']").set('QA TESTE AUTOMAÇÃO')
            find(:xpath, "//textarea[@name='P_INFORMACAO_FAM2']").set('QA TESTE AUTOMAÇÃO')
        end
    end

    def localizar_acessar_proposta_mesa(numero_proposta)
        find(:xpath, "//input[@type='search']").set(numero_proposta)
        find(:xpath, "//a[text()='#{numero_proposta}']").click
    end

    def pegar_valor_prazo_mesa
        @dados_proposta_sucesso = []

        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            @dados_proposta_sucesso << find(:xpath, "//input[@id='vl_prest']")[:value]
            @dados_proposta_sucesso << find(:xpath, "//input[@id='P_PROP_NUM_PREST']")[:value]
        end

        puts "Mesa - Proposta: " +  $numero_proposta + " Prazo: " + @dados_proposta_sucesso[1] + ' ' + "Valor: " + @dados_proposta_sucesso[0]
        return @dados_proposta_sucesso
    end

    def aprovar_proposta_mesa(numero_proposta)

        @texto_confirma_proposta_aprovada = ""

        within_window @nova_janela do
            localizar_acessar_proposta_mesa(numero_proposta)
            pop_up_alerta_mesa
            preencher_procedencia
            clicar_gravar_mesa
            accept_confirm("Os dados da proposta #{$numero_proposta} foram atualizados com sucesso.")
            pop_up_alerta_mesa
            pegar_valor_prazo_mesa
            aceitar_etapa_mesa
            aceitar_etapa_mesa
            preencher_checklist_mesa

            within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
                @texto_confirma_proposta_aprovada = find(:xpath, "//form/center/font[@class='txtAzulBold12']").text
            end
        end

        return  @texto_confirma_proposta_aprovada
    end

    def clicar_gravar_mesa
        send_keys [:control, :end]
        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            while !find(:xpath, "//input[@value='Gravar']").visible?
            end

            find(:xpath, "//input[@value='Gravar']").click
            page.accept_alert
        end
    end

    def aceitar_etapa_mesa
        send_keys [:control, :end]
        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            find(:xpath, "//input[contains(@onclick, 'APROVADA')]").click
            page.accept_alert
        end
    end

    def preencher_checklist_mesa
        within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
            all(:xpath, "//textarea[@class='texto']").each{|i| i.set("QA automação teste")}
            find(:xpath, "//input[@value='Validar']").click
            accept_confirm("Deseja finalizar a proposta?")
        end
    end
end