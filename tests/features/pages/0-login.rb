require_relative 'Util.rb'

class Login < Util

    set_url '/login'

    element :elementinputusuario, :xpath, "//input[@id='user-login']"
    element :elementinputsenha, :xpath, "//input[@id='user-password']"
    element :elementradioagente, :xpath, "//div/span[text()='184 - BATISTELLA & BATISTELLA LTDA']"
    element :elementspanentrar, :xpath, "//a/span[text()='Entrar']"
    element :elementbuttonconfirmar, :xpath, "//div/button[text()='Confirmar']"
    
    def preencher_usuario(usuario)
        elementinputusuario.set usuario
    end

    def preencher_senha(senha)
        elementinputsenha.set senha
    end

    def selecionar_agente
        elementradioagente.click
    end

    def confirmar_acesso
        elementbuttonconfirmar.click
    end

    def acessar_conta
        elementspanentrar.click
    end

    def selecionar_um_agente
        begin
            find(:xpath, "//button[text()='Confirmar']").click
        rescue Exception => ex
            ex.message
        end
    end

    def acessar_conta_omnimais_agente(usuario = $usuarioagente, senha = $senhaagente)
        preencher_usuario(usuario)
        preencher_senha(senha)
        acessar_conta
        selecionar_batistella
    end

    def acessar_conta_omnimais_loja(usuario = $usuarioloja, senha = $senhalojista)
        preencher_usuario(usuario)
        preencher_senha(senha)
        acessar_conta
        selecionar_um_agente
    end

    def selecionar_batistella
        selecionar_agente
        confirmar_acesso
    end
end