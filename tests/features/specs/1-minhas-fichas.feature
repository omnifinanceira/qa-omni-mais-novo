#language:pt

@1 @minhas-fichas
Funcionalidade: Validar campos minha ficha

Contexto:
    Dado que eu esteja logado como agente

Cenario: Verificar os campos existentes em minhas fichas
    Quando eu espero visualizar minhas fichas
    Entao valido as informacoes em cada ficha