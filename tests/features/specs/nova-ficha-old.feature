#language:pt
@nova-ficha-old
Funcionalidade: Passar uma nova ficha

Contexto:
    # Dado que eu esteja logado como agente
    Dado que eu esteja logado como lojista

Cenario: Criar nova proposta financiamento automoveis
Quando eu clico em Adicionar ficha automoveis financiamento old
E seleciono o tipo de operacao financiamento para a ficha automoveis old
E seleciono o solicitante para a ficha automoveis old
E seleciono o vendedor automoveis old
E insiro dados pessoais <cpf>, <nascimento> e <telefone> do cliente automoveis old
E insiro dados do veiculo <placa> automoveis old
E carrego o resultado parcial automoveis old
# E valido os dados ate o resultado parcial
E ajusto o valor financiado e quantidade de parcelas automoveis old
E insiro mais informacoes e <email> do cliente automoveis old
E insiro endereco do cliente automoveis old
E preencho observacoes automoveis old
Entao valido se a ficha foi criada e enviada para analise automoveis old
E validar os dados da proposta automoveis old
# Entao avalio proposta no omnifacil


Exemplos:
    |cpf          |nascimento  |placa    |telefone     |email                    |
    #moto
    # |"07418359837"|"18/04/1967"|"QPY7946"|"18991273552"|"naser65205@httptuan.com"|
    #carro
    |"07418359837"|"18/04/1967"|"NRF9720"|"18991273552"|"naser65205@httptuan.com"|
    # |"00585296839"|"11/12/1959"|"HTM1525"|
    # |"42053615800"|"07/09/1994"|"HTT0040"|
    # |"16601794830"|"06/07/1971"|"ASD7890"|
    # |"13982384818"|"05/01/1974"|"NRJ9316"|
    # |"25657215877"|"19/03/1976"|"ERV4580"|
    # |"12818545650"|"17/03/1995"|"DUD4638"|
    # |"26724399823"|"12/08/1978"|"PZX2453"|
    # |"09709130870"|"29/10/1968"|"PZX2453"|
    # |"26724399823"|"12/08/1978"|"MBO0006"|
    # |"09709130870"|"29/10/1968"|"EQQ5580"|
    #moto
    # |"26724399823"|"12/08/1978"|"AZQ2834"|
    # |"09709130870"|"29/10/1968"|"PXX8H86"|
    # |"26724399823"|"12/08/1978"|"BCJ2763"|
    # |"09709130870"|"29/10/1968"|"FSE6875"|