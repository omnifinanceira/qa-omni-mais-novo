#language:pt
@9 @refinanciamento-moto
Funcionalidade: Criar nova proposta refinanciamento moto

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Criar nova proposta refinanciamento moto
Quando eu clico em Adicionar ficha moto refi
E seleciono o tipo de operacao refinanciamento para a ficha moto
E seleciono o produto moto refi
E seleciono o solicitante para a ficha moto
E seleciono o vendedor moto refi
E insiro dados pessoais <cpf> e <nascimento> do cliente moto refi
E insiro dados do veiculo <placa> moto refi
E carrego o resultado parcial moto refi
E ajusto o valor financiado e quantidade de parcelas refi
E insiro mais informacoes do cliente moto refi
E insiro endereco do cliente moto refi
E preencho observacoes moto refi
Entao valido se a ficha foi criada e enviada para analise moto refi
E validar os dados da proposta moto refi

Exemplos:
    |cpf          |nascimento  |placa    |
    |"16601794830"|"06/07/1971"|"DXY3650"|