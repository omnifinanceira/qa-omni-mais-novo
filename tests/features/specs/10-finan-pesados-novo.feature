#language:pt
@10 @financiamento-pesados-novo
Funcionalidade: Criar nova proposta financiamento pesados

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Criacao proposta de pesados

Quando eu clico em Adicionar ficha pesado financiamento
E seleciono o "Financiamento", "Pesados" e "LOJISTA" pesado financiamento
E seleciono a "2 P VEICULOS" e o vendedor pesado financiamento
E insiro dados pessoais <cpf> e <nascimento> do cliente pesado financiamento
E insiro <avalista> ou <conjuge> pesado financiamento
E preencho informacoes do caminhao proprio pesado financiamento
E insiro dados do veiculo <placa> e quantidade de <garantia> pesado financiamento
E carrego o resultado parcial pesado financiamento
E valido as informacoes inseridas ate o resultado parcial pesado financiamento
E ajusto o seguro, valor financiado e quantidade de parcelas pesado financiamento
E insiro os detalhes das informacoes do cliente pesado financiamento
E insiro endereco e observacoes do cliente pesado financiamento
E finalizo a proposta e valido as informacoes financiamento pesado financiamento

Exemplos:
    |cpf          |nascimento  |placa    |garantia|avalista|conjuge|
    |"04333332869"|"06/08/1962"|"DTB9C03"|0       |0       |"false"|