#language:pt
@limite-global
Funcionalidade: Abertura conta IB

Cenario: Quando realizo abertura da conta com sucesso

Quando eu acessar o mock sms
    E buscar o numero do celular
    E buscar o link enviado e acessa-lo
    E iniciar a abertura da conta
    E digitar o cpf do cliente
    E aceito a mensagem de boas vindas
    E clico em continuar na tela de preenchimento da obrigação fiscal
    E inserir a data de vencimento
    E aceitar os termos
    Entao eu finalizo a abertura de conta