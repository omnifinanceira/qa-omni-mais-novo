#language:pt
@8 @refinanciamento-pesados
Funcionalidade: Criar nova proposta refinanciamento pesados

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Criar nova proposta refinanciamento pesados
Quando eu clico em Adicionar ficha pesado
E seleciono o tipo de operacao refinanciamento para a ficha pesado refinanciamento
E seleciono o produto pesado refinanciamento
E seleciono o solicitante para a ficha pesado refinanciamento
E seleciono a loja pesado refinanciamento
E seleciono o vendedor pesado refinanciamento
E insiro dados pessoais <cpf> e <nascimento> do cliente pesado refinanciamento
E preencho avalista refinanciamento
E preencho informacoes do caminhao proprio refinanciamento
E insiro dados do veiculo <placa> pesado refinanciamento
E carrego o resultado parcial pesado refinanciamento
E ajusto o valor financiado e quantidade de parcelas pesado refinanciamento
E insiro as informacoes de detalhes cliente refinanciamento
E informacoes dos caminhoes proprios do cliente refinanciamento
E informo os detalhes do avalista refinanciamento
E insiro endereco do cliente pesado refinanciamento
E informo o endereco do avalista refinanciamento
E preencho observacoes pesado refinanciamento
E preencho novo avalista refinanciamento
E finalizo a proposta refinanciamento
Entao valido se a ficha foi criada e enviada para analise pesado refinanciamento
E validar os dados da proposta pesado refinanciamento

Exemplos:
    |cpf          |nascimento  |placa    |
    |"04333332869"|"06/08/1962"|"DTB9C03"|
    # |"00585296839"|"11/12/1959"|"MHD0112"|
    # |"52171523891"|"03/06/1951"|"DWQ5170"|
    # |"42053615800"|"07/09/1994"|"APC0306"|
    # |"16601794830"|"06/07/1971"|"LKK8H06"|
    # |"13982384818"|"05/01/1974"|"AOM6D94"|
    # |"25657215877"|"19/03/1976"|"NGS8G11"|
    # |"12818545650"|"17/03/1995"|"EAC3B04"|
    # |"26724399823"|"12/08/1978"|"ACT9922"|
    # |"09709130870"|"29/10/1968"|"KYH0793"|