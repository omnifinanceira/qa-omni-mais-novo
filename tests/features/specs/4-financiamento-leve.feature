#language:pt
@4 @financiamento-leve
Funcionalidade: Criar nova proposta financiamento leve

Contexto:
    Dado que eu esteja logado como lojista

@leve-finalizar-proposta
Cenario: Criar nova proposta financiamento leve

Quando eu clico em Adicionar ficha pesado financiamento
E seleciono o "Financiamento", "Automóveis" e "LOJISTA" pesado financiamento
E seleciono a "2 P VEICULOS" e o vendedor pesado financiamento
E insiro dados pessoais <cpf> e <nascimento> do cliente leve financiamento
E insiro <avalista> ou <conjuge> pesado financiamento
E insiro dados do veiculo <placa> e quantidade de <garantia> pesado financiamento
E carrego o resultado parcial pesado financiamento
E valido as informacoes inseridas ate o resultado parcial pesado financiamento
E ajusto o seguro, valor financiado e quantidade de parcelas leve financiamento
# E informo os detalhes e endereco do avalista financiamento pesado financiamento
E insiro os detalhes das informacoes do cliente pesado financiamento
E insiro endereco e observacoes do cliente pesado financiamento
E finalizo a proposta e valido as informacoes financiamento pesado financiamento

Exemplos:
    |cpf          |nascimento  |placa    |garantia|avalista|conjuge|
    |"16601794830"|"06/07/1971"|"FHC9383"|0       |0       |"false"|