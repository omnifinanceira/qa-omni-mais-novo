#language:pt
@7 @refinanciamento-leve
Funcionalidade: Criar nova proposta refinanciamento leve

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Criar nova proposta refinanciamento leve
Quando eu clico em Adicionar ficha leve refi
E seleciono o tipo de operacao refinanciamento para a ficha leve 
E seleciono o produto leve refi
E seleciono o solicitante para a ficha leve refi
E seleciono as informacoes da loja e do vendedor leve refi
E insiro dados pessoais <cpf> e <nascimento> do cliente leve refi
E insiro dados do veiculo <placa> leve refi
E carrego o resultado parcial leve refi
E ajusto o valor financiado e quantidade de parcelas leve refi
E insiro mais informacoes do cliente leve refi
E insiro endereco do cliente leve refi
E preencho observacoes leve refi
Entao valido se a ficha foi criada e enviada para analise leve refi
E validar os dados da proposta leve refi


Exemplos:
    |cpf          |nascimento  |placa    |
    |"16601794830"|"06/07/1971"|"FHC9383"|