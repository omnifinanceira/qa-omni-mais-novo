#language:pt
@2 @opcoes-de-configuracoes
Funcionalidade: Validar opcoes de configuracoes

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Verificar botoes

Quando eu clico em opcoes de usuario
    E clico em configuracoes
    Entao eu valido as opcoes de configuracoes