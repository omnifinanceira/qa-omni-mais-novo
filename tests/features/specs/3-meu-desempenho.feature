#language:pt
@3 @meu-desempenho
Funcionalidade: Validar menu meu desempenho

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Verificar botoes

Quando eu clico em meu desempenho
    E valido as opcoes de filtros de desempenho
    E clico em Ranking
    Entao valido as opcoes de filtro de Ranking
