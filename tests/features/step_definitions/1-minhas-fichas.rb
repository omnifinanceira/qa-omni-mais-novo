Dado('que eu esteja logado como agente') do
    @login = Login.new
    @login.load
    @login.acessar_conta_omnimais_agente
    @login.selecionar_batistella
end

Quando('eu espero visualizar minhas fichas') do
    page.assert_selector(:xpath, "//app-ficha-em-analise/ol/li/app-ficha-card", wait:5)

    minhasfichas = all(:xpath, "//li/app-ficha-card/div/div[@class='body']/div[contains(@class, 'infos')]/div/div[text()]")
    expect(minhasfichas[0].text).to include('Ficha')
    expect(minhasfichas[1].text).to include('Iniciada')
end

Entao('valido as informacoes em cada ficha') do
    
end