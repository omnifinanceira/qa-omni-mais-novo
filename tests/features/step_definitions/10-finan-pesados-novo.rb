Quando('eu clico em Adicionar ficha pesado financiamento') do
    @sql = Queries.new
    @novaficha = NovaFicha.new
    @home = Home.new
    @novaficha.acessar_ficha
  end

  Quando('seleciono o {string}, {string} e {string} pesado financiamento') do |operacao, produto, solicitante|
    @novaficha.pegar_horario
    @novaficha.selecionar_opcao(operacao)
    @novaficha.selecionar_opcao(produto)
    @novaficha.selecionar_opcao(solicitante)
    @novaficha.clicar_em_continuar
  end

  Quando('seleciono a {string} e o vendedor pesado financiamento') do |loja|
    @novaficha.selecionar_loja(loja)
    @novaficha.selecionar_vendedor
    @novaficha.clicar_em_continuar
    
  end

  Quando('insiro dados pessoais {string} e {string} do cliente pesado financiamento') do |cpf, nascimento|
    @novaficha.preencher_dados_cliente(cpf,nascimento, true)
  end

  Quando('insiro {int} ou {string} pesado financiamento') do |avalista, conjuge|
    conjuge == "false" ? conjuge  = false : conjuge  = true
    @novaficha.clicar_adicionar_conjuge(conjuge)
    @avalista_preenchido = @novaficha.clicar_adicionar_avalista(avalista)
    @novaficha.clicar_em_continuar
  end

  Quando('preencho informacoes do caminhao proprio pesado financiamento') do 
    @novaficha.clicar_em_continuar
  end

  Quando('insiro dados do veiculo {string} e quantidade de {int} pesado financiamento') do |placa, garantia|
    @novaficha.preencher_dados_veiculo(placa)
    @novaficha.adicionar_garantia(garantia)
    @novaficha.clicar_em_continuar
  end

  Quando('carrego o resultado parcial pesado financiamento') do
    @novaficha.verificar_img_load
    @novaficha.verificar_pop_up_espera
  end

  Quando('valido as informacoes inseridas ate o resultado parcial pesado financiamento') do
    $numero_proposta = current_url.split('/').last
    dados_clientes_proposta = @sql.pegar_dados_clientes_proposta($numero_proposta)
    dados_clientes_proposta.each { |row| expect(!row.nil?).to be_truthy }
  
    dados_proposta_banco = @sql.pegar_dados_proposta_banco($numero_proposta)
    dados_proposta_banco.each { |row| expect(!row.nil?).to be_truthy }
  
    dados_clientes_telefones_proposta = @sql.pegar_dados_clientes_telefones_proposta($numero_proposta)
    dados_clientes_telefones_proposta.each { |row| expect(!row.nil?).to be_truthy }
  end

  Quando('ajusto o seguro, valor financiado e quantidade de parcelas pesado financiamento') do
    @novaficha.fechar_alerta_parcelamento
    # @novaficha.alterar_retorno_seguro_assistencia
    @novaficha.selecionar_valor_financiamento
    @novaficha.selecionar_parcelas
    @novaficha.pegar_dados_financiamento_simulador
    @novaficha.clicar_em_continuar
    @novaficha.fechar_pop_up_alerta_estrelas
  end

  Quando('insiro os detalhes das informacoes do cliente pesado financiamento') do
    @novaficha.preencher_detalhes_cliente(true)
    @novaficha.clicar_em_continuar
  end
  
  Quando('insiro endereco e observacoes do cliente pesado financiamento') do
    @novaficha.endereco_cliente
    @novaficha.endereco_cliente_pesado 
    @novaficha.clicar_em_continuar
    @novaficha.observacoes
  end
  
  Quando('finalizo a proposta e valido as informacoes financiamento pesado financiamento') do
    
    @novaficha.clicar_em_continuar
    @novaficha.verificar_img_load_analise
    
    @novaficha.pegar_dados_financiamento_finalizar

    texto_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text.split("\n")
      expect((texto_sucesso[0]).match("A FICHA #{$numero_proposta} FOI FINALIZADA")).to be_truthy
      expect((texto_sucesso[1]).match('E ENVIADA PARA ANÁLISE')).to be_truthy

    dados_proposta_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[4]/div/p").text.split(" ")
      expect((dados_proposta_sucesso[2].gsub('(','').gsub('x','')).match($quantidade_parcela_omni_mais)).to be_truthy
      expect((dados_proposta_sucesso[4]).gsub(')','').match($valor_parcela_omni_mais)).to be_truthy

    numero_proposta = find(:xpath, "//span[@class='id-proposta-info']").text
      expect((current_url.split('/').last).match(numero_proposta)).to be_truthy
  
    expect(find(:xpath, "//a[@href='/ficha'][text()='OK, ir para home ']").visible?).to be_truthy

    dados_clientes_fisica_proposta = @sql.pegar_dados_clientes_fisica_proposta($numero_proposta)
    dados_clientes_fisica_proposta.each { |row| expect(!row.nil?).to be_truthy }
  
    dados_clientes_enderecos_proposta = @sql.pegar_dados_clientes_enderecos_proposta($numero_proposta)
    dados_clientes_enderecos_proposta.each { |row| expect(!row.nil?).to be_truthy }

  end



