Quando('eu clico em Adicionar ficha moto refi') do
  @home = Home.new
  @home.acessar_ficha
end

Quando('seleciono o tipo de operacao refinanciamento para a ficha moto') do
  @novaficha = NovaFicha.new
  @novaficha.pegar_horario
  @novaficha.selecionar_opcao('Refinanciamento')
end

Quando('seleciono o produto moto refi') do
  @novaficha.pegar_horario
  @novaficha.selecionar_opcao('Motocicletas')
end

Quando('seleciono o solicitante para a ficha moto') do
  @novaficha.selecionar_opcao('CLIENTE NO BALCÃO')
  @novaficha.clicar_em_continuar
end

Quando('seleciono o vendedor moto refi') do
  @novaficha.pegar_horario
  @novaficha.selecionar_vendedor
  @novaficha.clicar_em_continuar
end

Quando('insiro dados pessoais {string} e {string} do cliente moto refi') do |cpf, nascimento|
  @novaficha.preencher_dados_cliente(cpf,nascimento, false)
  @novaficha.clicar_em_continuar
end

Quando('insiro dados do veiculo {string} moto refi') do |placa|
  @novaficha.preencher_dados_veiculo(placa)
  @novaficha.fechar_alerta_cpf_ficha_existente
  @novaficha.clicar_em_continuar
end

Quando('carrego o resultado parcial moto refi') do
  @novaficha.verificar_img_load
  @novaficha.fechar_alerta_parcelamento
end

Quando('ajusto o valor financiado e quantidade de parcelas refi') do
  @novaficha.selecionar_valor_financiamento
  @novaficha.selecionar_parcelas
  @novaficha.pegar_dados_financiamento
  @novaficha.clicar_em_continuar
end

Quando('insiro mais informacoes do cliente moto refi') do
  @novaficha.preencher_detalhes_cliente
  @novaficha.clicar_em_continuar
end

Quando('insiro endereco do cliente moto refi') do
  @novaficha.endereco_cliente
  @novaficha.clicar_em_continuar
end

Quando('preencho observacoes moto refi') do
  @novaficha.observacoes
  @novaficha.clicar_em_continuar
end

Entao('valido se a ficha foi criada e enviada para analise moto refi') do
  teste = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text

  texto_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text.split("\n")
    expect((texto_sucesso[0]).match('A SOLICITAÇÃO FOI FINALIZADA')).to be_truthy
    expect((texto_sucesso[1]).match('E ENVIADA PARA ANÁLISE')).to be_truthy
end

Entao('validar os dados da proposta moto refi') do
  dados_proposta_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[4]/div/p").text.split(" ")
  expect((dados_proposta_sucesso[2].gsub('(','').gsub('x','')).match($quantidade_parcela_omni_mais)).to be_truthy
  expect((dados_proposta_sucesso[4]).gsub(')','').match($valor_parcela_omni_mais)).to be_truthy

find(:xpath, "//a[@href='/ficha'][text()='OK, ir para home ']").click
end