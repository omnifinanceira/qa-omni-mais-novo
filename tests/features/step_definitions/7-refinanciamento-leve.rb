Quando('eu clico em Adicionar ficha leve refi') do
  @home = Home.new
  @home.acessar_ficha
end

Quando('seleciono o tipo de operacao refinanciamento para a ficha leve') do
  @novaficha = NovaFicha.new
  @novaficha.pegar_horario
  @novaficha.selecionar_opcao('Refinanciamento')
end

Quando('seleciono o produto leve refi') do
  @novaficha.pegar_horario
  @novaficha.selecionar_opcao('Automóveis')
end

Quando('seleciono o solicitante para a ficha leve refi') do
  @novaficha.selecionar_opcao('LOJISTA')
  @novaficha.clicar_em_continuar
end

Quando('seleciono as informacoes da loja e do vendedor leve refi') do
  @novaficha.pegar_horario
  @novaficha.selecionar_loja
  @novaficha.selecionar_vendedor
  @novaficha.clicar_em_continuar
end

Quando('insiro dados pessoais {string} e {string} do cliente leve refi') do |cpf,nascimento|
  @novaficha.preencher_dados_cliente(cpf,nascimento)
  @novaficha.clicar_em_continuar
end

Quando('insiro dados do veiculo {string} leve refi') do |placa|
  if placa == ""
    sql = Queries.new
    placa =  sql.pegar_placa('leve')
  end

  @novaficha.preencher_dados_veiculo(placa)
  @novaficha.fechar_alerta_cpf_ficha_existente
  @novaficha.clicar_em_continuar
end

Quando('carrego o resultado parcial leve refi') do
  @novaficha.verificar_img_load
  @novaficha.fechar_alerta_parcelamento
end

Quando('ajusto o valor financiado e quantidade de parcelas leve refi') do
  @novaficha.pegar_numero_ficha
  @novaficha.selecionar_valor_financiamento
  @novaficha.selecionar_parcelas
  @novaficha.pegar_dados_financiamento
  @novaficha.clicar_em_continuar
end

Quando('insiro mais informacoes do cliente leve refi') do
  @novaficha.preencher_detalhes_cliente
  @novaficha.clicar_em_continuar
end

Quando('insiro endereco do cliente leve refi') do
  @novaficha.endereco_cliente
  @novaficha.clicar_em_continuar
end

Quando('preencho observacoes leve refi') do
  @novaficha.observacoes
  @novaficha.clicar_em_continuar
end

Entao('valido se a ficha foi criada e enviada para analise leve refi') do
    teste = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text

    texto_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text.split("\n")
      expect((texto_sucesso[0]).match('A SOLICITAÇÃO FOI FINALIZADA')).to be_truthy
      expect((texto_sucesso[1]).match('E ENVIADA PARA ANÁLISE')).to be_truthy
end

Entao('validar os dados da proposta leve refi') do
  dados_proposta_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[4]/div/p").text.split(" ")
  expect((dados_proposta_sucesso[2].gsub('(','').gsub('x','')).match($quantidade_parcela_omni_mais)).to be_truthy
  expect((dados_proposta_sucesso[4]).gsub(')','').match($valor_parcela_omni_mais)).to be_truthy

find(:xpath, "//a[@href='/ficha'][text()='OK, ir para home ']").click
end