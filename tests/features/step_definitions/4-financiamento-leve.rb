Dado('que eu esteja logado como lojista') do
  @login = Login.new
  @login.load
  @login.acessar_conta_omnimais_loja
  @sql = Queries.new
  @omnifacil = OmniFacil.new
end

Quando('eu clico em Adicionar ficha leve financiamento') do
  @home = Home.new
  @home.acessar_ficha
end

Quando('seleciono o tipo de operacao financiamento para a ficha leve') do
  @novaficha = NovaFicha.new
  @novaficha.pegar_horario
  @novaficha.selecionar_opcao('Financiamento')
end

Quando('seleciono o produto leve') do
  @novaficha.pegar_horario
  @novaficha.selecionar_opcao('Automóveis')
end

Quando('seleciono o solicitante para a ficha leve') do
  @novaficha.selecionar_opcao('LOJISTA')
  @novaficha.clicar_em_continuar
end

Quando('seleciono a loja leve') do
  @novaficha.selecionar_loja
end

Quando('seleciono o vendedor leve') do
  @novaficha.pegar_horario
  @novaficha.selecionar_vendedor
  @novaficha.clicar_em_continuar
end

Quando('insiro dados pessoais {string} e {string} do cliente leve') do |cpf,nascimento|
  @novaficha.preencher_dados_cliente(cpf,nascimento)
  @novaficha.clicar_em_continuar
end

Quando('insiro dados do veiculo {string} leve') do |placa|
  if placa == ""
    sql = Queries.new
    placa =  sql.pegar_placa('leve')
  end

  @novaficha.preencher_dados_veiculo(placa)
  @novaficha.fechar_alerta_cpf_ficha_existente
  @novaficha.clicar_em_continuar
end

Quando('carrego o resultado parcial leve') do
  @novaficha.verificar_img_load
  @novaficha.verificar_pop_up_espera
end

Quando('ajusto o valor financiado e quantidade de parcelas leve') do
  @novaficha.fechar_alerta_parcelamento
  @novaficha.selecionar_valor_financiamento
  @novaficha.selecionar_parcelas
end

Quando('ajusto as preferencias do seguro leve') do
  # @novaficha.alterar_retorno_seguro_assistencia
end

Quando('valido as alteracoes e capturo os valores') do
  @novaficha.pegar_dados_financiamento
  @novaficha.clicar_em_continuar
  @novaficha.fechar_pop_up_alerta_estrelas
end

Quando('insiro mais informacoes do cliente leve') do
  @novaficha.preencher_detalhes_cliente
  @novaficha.clicar_em_continuar
end

Quando('insiro endereco do cliente leve') do
  @novaficha.endereco_cliente
  @novaficha.clicar_em_continuar
end

Quando('preencho observacoes leve') do
  @novaficha.observacoes
  @novaficha.clicar_em_continuar
end

Entao('valido se a ficha foi criada e enviada para analise leve') do
  teste = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text

    texto_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text.split("\n")
      expect((texto_sucesso[0]).match("A FICHA #{$numero_proposta} FOI FINALIZADA")).to be_truthy
      expect((texto_sucesso[1]).match('E ENVIADA PARA ANÁLISE')).to be_truthy
end

Entao('valido os dados financeiro da proposta leve') do
  dados_proposta_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[4]/div/p").text.split(" ")
    expect((dados_proposta_sucesso[2].gsub('(','').gsub('x','')).match($quantidade_parcela_omni_mais)).to be_truthy
    expect((dados_proposta_sucesso[4]).gsub(')','').match($valor_parcela_omni_mais)).to be_truthy

  expect(find(:xpath, "//a[@href='/ficha'][text()='OK, ir para home ']").visible?).to be_truthy
end

Entao('valido os dados gravado no banco leve') do
  dados_clientes_fisica_proposta = @sql.pegar_dados_clientes_fisica_proposta($numero_proposta)
  dados_clientes_fisica_proposta.each { |row| expect(!row.nil?).to be_truthy }

  dados_clientes_enderecos_proposta = @sql.pegar_dados_clientes_enderecos_proposta($numero_proposta)
  dados_clientes_enderecos_proposta.each { |row| expect(!row.nil?).to be_truthy }
end

Entao('acesso o omnifacil leve') do
  @omnifacil.load
  @omnifacil.logar_usuario($usuarioloja,$senhalojista)
  @omnifacil.selecionar_menu_inicial(" NOVO MENU")
  @omnifacil.selecionar_menu('OPERACIONAL')
  @omnifacil.selecionar_submenu('CRÉDITO')
  @omnifacil.selecionar_submenu('Fila')
  @omnifacil.consultar_ficha($numero_proposta)
end

Entao('envio a proposta para a mesa leve') do
end

Entao('acesso a mesa leve') do
  @omnifacil.load
  @omnifacil.logar_usuario("ANTONIO_RODRIGUES","MOTOS2021")
  @omnifacil.selecionar_menu_inicial("MESA DE CRÉDITO - (Nova Fila) ")
end

Quando('insiro dados pessoais {string} e {string} do cliente leve financiamento') do |cpf, nascimento|
  @novaficha.preencher_dados_cliente(cpf,nascimento, false)
end

Quando('ajusto o seguro, valor financiado e quantidade de parcelas leve financiamento') do
  @novaficha.fechar_alerta_parcelamento
  # @novaficha.alterar_retorno_seguro_assistencia
  @novaficha.selecionar_valor_financiamento
  @novaficha.selecionar_parcelas
  @novaficha.pegar_dados_financiamento_simulador
  @novaficha.clicar_em_continuar
  @novaficha.fechar_pop_up_alerta_estrelas
end