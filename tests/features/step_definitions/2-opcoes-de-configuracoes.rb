Quando('eu clico em opcoes de usuario') do
    @home = Home.new
  end
  
  Quando('clico em configuracoes') do
    @home.acessar_opcoes_de_usuario
    @home.acessar_configuracoes
  end
  
  Entao('eu valido as opcoes de configuracoes') do
    expect(all(:xpath, "//input[@type='checkbox']")[0]['ng-reflect-model']) == true
    expect(all(:xpath, "//input[@type='checkbox']")[1]['ng-reflect-model']) == true
    expect(all(:xpath, "//input[@type='checkbox']")[2]['ng-reflect-model']) == true
    expect(all(:xpath, "//input[@type='checkbox']")[3]['ng-reflect-model']) == true
  end