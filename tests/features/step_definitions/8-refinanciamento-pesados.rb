Quando('seleciono o tipo de operacao refinanciamento para a ficha pesado refinanciamento') do
  @novaficha = NovaFicha.new
  @novaficha.pegar_horario
  @novaficha.selecionar_opcao('Refinanciamento')
end

Quando('seleciono o produto pesado refinanciamento') do
  @novaficha.pegar_horario
  @novaficha.selecionar_opcao('Pesados')
end

Quando('seleciono o solicitante para a ficha pesado refinanciamento') do
  @novaficha.selecionar_opcao('LOJISTA')
  @novaficha.clicar_em_continuar
end

Quando('seleciono a loja pesado refinanciamento') do
  @novaficha.selecionar_loja
end

Quando('seleciono o vendedor pesado refinanciamento') do
  @novaficha.pegar_horario
  @novaficha.selecionar_vendedor
  @novaficha.clicar_em_continuar
end

Quando('insiro dados pessoais {string} e {string} do cliente pesado refinanciamento') do |cpf, nascimento|
  @novaficha.preencher_dados_cliente(cpf,nascimento, true)
end

Quando('preencho avalista refinanciamento') do
  @novaficha.clicar_adicionar_avalista
  @novaficha.preencher_avalista_cliente
  @novaficha.preencher_veiculo_avalista
  @novaficha.clicar_em_continuar
end

Quando('preencho informacoes do caminhao proprio refinanciamento') do
  placa =  @sql.pegar_placa('pesado')
  @novaficha.marcar_caminhao_proprio_sim(true)
  @novaficha.quantidade_caminhao_proprio
  @novaficha.placa_caminhao_proprio(placa)
  @novaficha.clicar_em_continuar
end

Quando('insiro dados do veiculo {string} pesado refinanciamento') do |placa|
  @novaficha.preencher_dados_veiculo(placa)
  @novaficha.fechar_alerta_cpf_ficha_existente
  @novaficha.clicar_em_continuar
end

Quando('carrego o resultado parcial pesado refinanciamento') do
  @novaficha.verificar_img_load
  @novaficha.fechar_alerta_parcelamento
end

Quando('ajusto o valor financiado e quantidade de parcelas pesado refinanciamento') do
  @novaficha.pegar_numero_ficha

  # dados_banco_proposta = @sql.pegar_dados_proposta_banco($numero_proposta)

  #DADOS PROPOSTA
  # dados_banco_proposta.each { |row| expect(!row.nil?).to be_truthy }

  @novaficha.selecionar_valor_refinanciamento
  @novaficha.selecionar_parcelas
  @novaficha.pegar_dados_refinanciamento
  @novaficha.clicar_em_continuar
end

Quando('insiro as informacoes de detalhes cliente refinanciamento') do
  @novaficha.preencher_detalhes_cliente(true)
  @novaficha.clicar_em_continuar
end

Quando('informacoes dos caminhoes proprios do cliente refinanciamento') do
  @novaficha.preencher_informacoes_caminhoes(false,true)
  @novaficha.clicar_em_continuar
end

Quando('informo os detalhes do avalista refinanciamento') do
  @novaficha.preencher_detalhes_avalista
  @novaficha.clicar_em_continuar
end

Quando('insiro endereco do cliente pesado refinanciamento') do
  @novaficha.endereco_cliente
  @novaficha.clicar_em_continuar
end

Quando('informo o endereco do avalista refinanciamento') do
  @novaficha.endereco_avalista
  @novaficha.clicar_em_continuar
end

Quando('preencho observacoes pesado refinanciamento') do
  @novaficha.observacoes
end

Quando('preencho novo avalista refinanciamento') do
  @novaficha.clicar_adicionar_avalista_observacoes
  @novaficha.preencher_avalista_observacoes
  @novaficha.clicar_salvar
end

Quando('finalizo a proposta refinanciamento') do
  @novaficha.clicar_em_continuar
  @novaficha.verificar_img_load_analise
end

Entao('valido se a ficha foi criada e enviada para analise pesado refinanciamento') do
  teste = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text

  texto_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[1]/h3").text.split("\n")
    expect((texto_sucesso[0]).match('A SOLICITAÇÃO FOI FINALIZADA')).to be_truthy
    expect((texto_sucesso[1]).match('E ENVIADA PARA ANÁLISE')).to be_truthy
end

Entao('validar os dados da proposta pesado refinanciamento') do
  dados_proposta_sucesso = find(:xpath, "//app-ficha-cadastro-analise/span/div[1]/div/div[4]/div/p").text.split(" ")
  expect((dados_proposta_sucesso[2].gsub('(','').gsub('x','')).match($quantidade_parcela_omni_mais)).to be_truthy
  expect((dados_proposta_sucesso[4]).gsub(')','').match($valor_parcela_omni_mais)).to be_truthy

find(:xpath, "//a[@href='/ficha'][text()='OK, ir para home ']").click
end